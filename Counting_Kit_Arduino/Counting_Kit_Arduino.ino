/*REQUIRED LIBRARIES FOR THE ARDUINO SKETCH*/
#include <Arduino.h>                  // Core library for Arduino. It includes essential definitions and functions used throughout the Arduino environment.
#include <WiFi.h>                     // Enables the Arduino board to connect to a Wi-Fi network and interact with it.
#include <Ticker.h>                   // Call functions at specified intervals. Often used for creating periodic tasks or timers.
#include <Preferences.h>              // Save and retrieve persistent data in the ESP32's non-volatile memory.
#include <AsyncElegantOTA.h>          // Implementing over-the-air (OTA) updates for the Arduino sketch. Update the sketch wirelessly.
#include <AsyncTCP.h>                 // Support for asynchronous TCP connections. Combination with the AsyncWebServer library.
#include <ESPAsyncWebServer.h>        // Handle HTTP requests and build web-based user interfaces.
#include <WebSerialLite.h>            // Enables web-based serial communication between the Arduino board and a web browser
#include <MQTT.h>                     // The MQTT PubSubClient library is replaced by the newest one: arduino-mqtt (https://github.com/256dpi/arduino-mqtt)
#include <Wire.h>                     // Allows the Arduino to communicate with other devices using the I2C protocol.
#include "EasyNextionLibrary.h"       // Custom library for Nextion display
/*_______________________________________________________________________________________________________________________________________________________________________________________________________*/

/*PREPROCESSOR DIRECTIVES USED TO CREATE SYMBOLIC CONSTANTS*/

// Firmware version, can be used to identify the current version of the firmware.
#define FW_version                    "v1.D"

// Page names for the Nextion display. These are used to navigate between different pages on the display.
#define SPLASH_SCREEN                 "page page0"
#define SENSING_DEVICE                "page page1"
#define HOME_COUNTING                 "page page2"
#define HOME_LOADCELL                 "page page3"
#define WARNING_BACK                  "page page4"
#define WARNING_CONFIG                "page page5"
#define CONFIG_COUNT                  "page page6"
#define CONFIG_LC                     "page page7"
#define SAVE_CONFIG                   "page page8"
#define STATUS_PAGE                   "page page9"
#define DEBUG_PAGE                    "page page10"

// Intervals and time periods in milliseconds.
#define PAGE_REFRESH_INTERVAL         500                         // Interval for refreshing the display pages.
#define MQTT_CHECK_INTERVAL           1000                        // Interval for checking MQTT connectivity.
#define SEND_CONFIGURATION_INTERVAL   5000                        // Interval for sending configuration data.

#define PASSWORD                      "1"                         // Default password for authentication.
#define WRONG_PASS_MSG                "Wrong Password Entered"    // Message to show when the wrong password is entered.

// Counting Kit states (State machine). These are used to represent different states of the program.
#define STATE_ERROR                   0
#define STATE_INIT                    1
#define STATE_CONNECT_WIFI            2
#define STATE_CONNECT_MQTT            3
#define STATE_SENSING_DEVICE          4
#define STATE_WAITING                 5
#define STATE_HOME_COUNTING           6
#define STATE_HOME_LOADCELL           7

// Error definitions. These are used to represent different error states.
#define ERROR_NO_ERROR                0
#define ERROR_CONNECT_WIFI            1
#define ERROR_CONNECT_MQTT            2
#define ERROR_SUB_MQTT                3
#define ERROR_PUB_MQTT                4

// Base Station or Machine status definitions. These represent different states of the machine.
#define NoProduction                  0
#define Production                    1
#define MachineSetup                  2
/*_______________________________________________________________________________________________________________________________________________________________________________________________________*/

/*VARIABLES AND THEIR INITIALIZATIONS*/

// MQTT Message Offset
unsigned int CKIdOffset = 16;                             // Offset for CK.Id in the received MQTT message.
unsigned int MachineStatusOffset = 32;                    // Offset for TPC.MachineStatus in the received MQTT message.

// ESP32 Board pins that can be connected with sensor
const byte sensor1Pin = 18;                               // Pin number for sensor 1.
const byte sensor2Pin = 19;                               // Pin number for sensor 2.

// Counters and timers
unsigned int secondCounter = 0;                           // Counter to control specific actions within Nextion Display with a time delay
unsigned long pageRefreshTimer = 0;                       // Timer for refreshing the Nextion display pages
unsigned long SendConfigurationTimer = 0;                 // Timer for sending CK configuration to Base Station via CK.Discover
uint32_t lastMillis_waiting_state = 0;                    // Variable to store the last time in milliseconds for waiting state

//Variable related to WiFi connectivity
volatile bool webSerialAvailable = false;                 // Flag indicating if WebSerial is available (volatile for ISR access)

//Variable related to MQTT publishing at case STATE_HOME_COUNTING
unsigned long publishMillis;                              // Time marker for publishing MQTT messages
unsigned long publishMQTTInterval = 2000;                 // Interval for publishing MQTT messages
unsigned long prevMQTTReconnectAttempt = 0;               // Variable to store the previous MQTT reconnection attempt time
unsigned long MQTTReconnectInterval = 5000;               // Interval for attempting to reconnect to MQTT broker

// Variables for Wi-Fi configurations.
String WIFI_SSID = "";                                    // Variable to get or insert Wi-Fi SSID (Base Station Router)
char ssid_buffer[50] = "";                                // Buffer for storing WiFi SSID as a C-string
String WIFI_Password = "";                                // Variable to get or insert WiFi password (Base Station Router)
char pass_buffer[50] = "";                                // Buffer for storing WiFi password as a C-string

// Variables for MQTT configurations.
String MQTT_Broker = "";                                  // Variable to get or insert MQTT broker ip address
char mqttBroker_buffer[50];                               // Buffer for storing MQTT broker address as a C-string
String MQTT_Port = "";                                    // Variable to get or insert MQTT broker port

// Variables for other configurations.
String CountUnit = "";                                    // Count unit for display, normally in 'pcs'
String WeightUnit = "";                                   // Weight unit for display, normally in 'kg'
String WarningPassword = "";                              // Password for configuring warnings
String prevPage = "";                                     // Variable to store page name for Nextion page as previous page
String lastOperation = "";                                // Variable to store the last operation or page performed
String MQTTMessage = "";                                  // MQTT message received
String CK_OWN_ID = "";                                    // Variable to store or write ID for CK device
char ck_id_buffer[50] = "";                               // Buffer for storing CK device ID as a C-string
bool isConfigured;                                        // Flag indicating if the Counting Kit device is configured

// Variables for Digital CK configurations
float calculatedCount = 0;                                // Variable to store and use when computing calculated count value
float oldCalculatedCount = 0;                             // Variable to store calculated count value and use it to do comparison in if statement
float countMultiplier = 1;                                // Multiplier for raw count to calculate the final count value
volatile unsigned long rawCount = 0;                      // Variable to update count of signal(Raw) value (volatile for ISR access)

// Variables for Load Cell CK configurations
float calculatedWeight = 0;                               // Variable to store and use when computing calculated weight value
float maxValue = 0;                                       // Variable to store the maximum value of accepted weight as pass count
float minValue = 0;                                       // Variable to store the minimum value of accepted weight as pass count

// Variables as a state indicators (State Machine, Error)
uint8_t current_state = STATE_INIT;                       // Variable to indicate current state in the state machine (initialized to STATE_INIT)
uint8_t error_code = ERROR_NO_ERROR;                      // Variable that indicates error code (initialized to ERROR_NO_ERROR)

// Variables use for debugging
uint8_t debug = 1;                                        // Debug flag for enabling/disabling debug messages via Serial (1: enabled, 0: disabled)
uint8_t debug_debounce = 0;                               // Debug debounce flag for enabling/disabling measure debounce time (1: enabled, 0: disabled)
uint8_t inByte;                                           // Variable to store incoming byte from Serial for debug purposes

// Variables to store ESP32 unique identifier
String esp32_macAddress;                                  // ESP32 MAC address (initialized as an empty string)
String esp32_IPAddress;                                   // ESP32 IP address (initialized as an empty string)

// Variables for storing data received via MQTT
String OEEdata_copy;                                      // Copy of OEE data received via MQTT (initialized as an empty string)
String SubOEEdata;                                        // Substring of OEE data (initialized as an empty string)
int indexComma;                                           // Index of comma in OEE data (initialized as 0)
bool CK_Pass_flag = false;                                // Flag to indicate if CK is a "pass" CK (initialized as false)
bool CK_Interlock_flag = false;                           // Flag to indicate if CK has "interlock" enabled (initialized as false)
String CKCountTopic;                                      // Topic for publishing CK count (initialized as an empty string)

// Variables related to Sensor 1
int buttonState = HIGH;                                   // Current reading from the input pin for sensor 1 (initialized as HIGH)
int lastButtonState = HIGH;                               // Previous reading from the input pin for sensor 1 (initialized as HIGH)
unsigned long lastDebounceTime = 0;                       // Last time the output pin for Sensor 1 was triggered
unsigned long debounceDelay = 10;                         // The debounce time or delay for Sensor 1 (increase if the output flickers)

// Variables related to Sensor 2
int buttonState2 = HIGH;                                  // Current reading from the input pin for sensor 2 (initialized as HIGH)
int lastButtonState2 = HIGH;                              // Previous reading from the input pin for sensor 2 (initialized as HIGH)
unsigned long lastDebounceTime2 = 0;                      // Last time the output pin for Sensor 2 was triggered
unsigned long debounceDelay2 = 10;                        // The debounce time or delay for Sensor 2 (increase if the output flickers)

// Variables use as an indicator or flag
volatile bool webSerialMsgAvailable = false;              // Flag to check if a WebSerial message is available (volatile for use in an ISR)
volatile bool wifiConnected = false;                      // Flag to indicate if WiFi is connected (volatile for use in an ISR)
volatile bool mqttConnected = false;                      // Flag to indicate if MQTT is connected (volatile for use in an ISR)
volatile bool inProduction = false;                       // Flag to indicate if the base station is in production mode (volatile for use in an ISR)
volatile int currentMachineStatus;                        // Variable to store the current status of the machine whether MachineSetup, NoProduction or Production (volatile for use in an ISR)
/*_______________________________________________________________________________________________________________________________________________________________________________________________________*/

/*CREATING INSTANCE LIBRARY*/

EasyNex myNex(Serial2);                                   // Create an instance of the EasyNex class, named myNex, and initialize it with the Serial2 object.
                                                          // The EasyNex library is used to communicate with the Nextion display.

Preferences memory;                                       // Create an instance of the Preferences class, named memory.
                                                          // The Preferences library allows you to store and retrieve key-value pairs in non-volatile memory (NVRAM).

WiFiClient espClient;                                     // Create an instance of the WiFiClient class, named espClient.
                                                          // The WiFiClient library provides functions to establish a connection to a Wi-Fi network.

MQTTClient client(512);                                   // Create an instance of the MQTTClient class, named client, and initialize it with a buffer size of 512.
                                                          // The MQTTClient library allows you to connect to an MQTT broker and publish/subscribe to topics.

AsyncWebServer server(80);                                // Create an instance of the AsyncWebServer class, named server, and initialize it to listen on port 80.
                                                          // The AsyncWebServer library enables you to create a web server and handle HTTP requests asynchronously.

// Overall Comment:
// The above objects are essential components of the program. They facilitate communication with the Nextion display,
// store configuration data in non-volatile memory, handle Wi-Fi connectivity, interact with an MQTT broker, and set up a web server.
// Each object is used in various parts of the program to perform specific tasks.
/*_______________________________________________________________________________________________________________________________________________________________________________________________________*/

/*CALLING PROGRAM FUNCTION - WiFiGotIP*/

// WiFiGotIP: Event handler for successful WiFi connection and IP address acquisition.
void WiFiGotIP(WiFiEvent_t event, WiFiEventInfo_t info)
{
  WebSerial.begin(&server);                                               // Initialize WebSerial to enable communication through a web-based Serial Monitor.
  WebSerial.onMessage(recvMsg);                                           // Set the message handler function for WebSerial.
  AsyncElegantOTA.begin(&server);                                         // Initialize AsyncElegantOTA for Over-The-Air (OTA) updates.
  server.begin();// Start the web server to handle incoming requests.
  Serial.println("IP address: ");                                         // Retrieve and display the IP address of the ESP32.
  esp32_IPAddress = WiFi.localIP().toString();
  Serial.println(esp32_IPAddress);
  esp32_macAddress = WiFi.macAddress();                                   // Retrieve and format the MAC address of the ESP32 to use as a unique ID for the CK.
  esp32_macAddress.replace(":", "");
  CK_OWN_ID = "CK" + esp32_macAddress;
  memory.putString("ck-id", CK_OWN_ID);                                   // Store the CK ID in the non-volatile memory (Preferences).
  CK_OWN_ID.toCharArray(ck_id_buffer, CK_OWN_ID.length() + 1);            // Convert the CK ID to a char array for further use if needed.
  webSerialAvailable = true;                                              // Set the WebSerial availability flag to true.
  error_code = 0;                                                         // Reset the error code to indicate no error occurred.
  current_state = STATE_CONNECT_MQTT;                                     // Set the current state to STATE_CONNECT_MQTT to initiate MQTT connection.
  wifiConnected = true;                                                   // Set the wifiConnected flag to true to indicate a successful WiFi connection.
  MQTT_Broker.toCharArray(mqttBroker_buffer, MQTT_Broker.length() + 1);   // Convert the MQTT Broker address to a char array for further use if needed.
  Serial.println("Beginning MQTT Client.");                               // Print a message indicating the beginning of MQTT client setup.
  client.begin(mqttBroker_buffer, espClient);                             // Start the MQTT client by providing the broker address and ESP client instance.
  Serial.println("Register message callback event.");                     // Print a message indicating the register message callback event.
  client.onMessageAdvanced(MQTTcallback);                                 // Register the MQTT callback function to handle incoming MQTT messages.
}
/*_______________________________________________________________________________________________________________________________________________________________________________________________________*/

/*CALLING PROGRAM FUNCTION - WiFiConnected*/

// WiFiConnected: Event handler indicating successful connection of the CK to the OEE Base Station WiFi.
void WiFiConnected(WiFiEvent_t event, WiFiEventInfo_t info)
{
  Serial.println("CK connected to OEE Base Station Wifi");                // Print a message to indicates the CK wifi is connected to the BS.
}
/*_______________________________________________________________________________________________________________________________________________________________________________________________________*/

/*CALLING PROGRAM FUNCTION - WiFiDisconnected*/

// WiFiDisconnected: Event handler indicating disconnection from the OEE Base Station WiFi.
void WiFiDisconnected(WiFiEvent_t event, WiFiEventInfo_t info)
{
  Serial.println("CK disconnected from OEE Base Station WiFi");           // Print a message as indicator
  Serial.println("WiFi lost connection.");
  Serial.println("Possible Reason:-");                                    // Print a message to indicates possible reason of losing WiFi connection.
  Serial.println("1) Signal Interference:");
  Serial.println("Interference from other electronic devices or");
  Serial.println("obstacles causing weak or unstable Wi-Fi signals.");
  Serial.println("2) Distance from Access Point:");
  Serial.println("Being too far from the Wi-Fi access point,");
  Serial.println("resulting in a weak connection.");
  Serial.println("3) Power Issues:");
  Serial.println("Inadequate power supply or power fluctuations");
  Serial.println("affecting Wi-Fi stability.");
  Serial.println("4) Router Problems:");
  Serial.println("Firmware issues, configuration errors, or");
  Serial.println("an overloaded router can cause disconnections.");
  Serial.println("5) Network Congestion:");
  Serial.println("Too many connected devices on the Wi-Fi");
  Serial.println("network leading to instability.");
  Serial.println("6) Channel Overlap:");
  Serial.println("Multiple Wi-Fi networks using the same channel");
  Serial.println("causing interference.");
  Serial.println("7) Authentication or Encryption Errors:");
  Serial.println("Incorrect Wi-Fi credentials or encryption");
  Serial.println("settings preventing connection.");  
  Serial.println("8) Sleep Modes:");
  Serial.println("Using low-power sleep modes may cause the ESP32");
  Serial.println("to disconnect from Wi-Fi to save power.");
  Serial.println("9) Firmware or Software Bugs:");
  Serial.println("Bugs in ESP32 firmware or application code");
  Serial.println("leading to Wi-Fi connection issues.");
  Serial.println("");
  Serial.println("Trying to Reconnect");                                  // Print a message as indicator to try reconnecting to WiFi network.
  WiFi.reconnect();                                                       // Function calls to attempt reconnecting to the WiFi network.
  wifiConnected = false;                                                  // Set variable, device is currently not connected to the Wifi
  webSerialAvailable = false;                                             // Set variable, device is not available for web serial communication.
}
/*_______________________________________________________________________________________________________________________________________________________________________________________________________*/

/*CALLING PROGRAM FUNCTION - recvMsg*/

// Function to handle incoming data from Web Serial connection
void recvMsg(uint8_t *data, size_t len) 
{
  WebSerial.println("Received Data...");                                  // Print a message to indicate that data has been received
  String d = "";                                                          // Initialize an empty string to store the received data
  for (int i = 0; i < len; i++)                                           // Loop through the received data bytes
  {
    d += char(data[i]);                                                   // Convert each byte to a character and append it to the data string
  }
  WebSerial.println(d);                                                   // Print the received data to the Web Serial console
  webSerialMsgAvailable = true;                                           // Set the flag to indicate that new data is available for processing
}

// Overall Comment:
// This function, recvMsg, is a callback that handles incoming data from the Web Serial connection.
// When data is received, it reconstructs the data as a string and prints it to the Web Serial console.
// The boolean variable webSerialMsgAvailable is set to true to notify the main program that new data is available for processing
/*_______________________________________________________________________________________________________________________________________________________________________________________________________*/

/*CALLING PROGRAM FUNCTION - MQTTcallback*/

// This function is called whenever an MQTT message is received.
void MQTTcallback(MQTTClient *client, char topic[], char payload[], int payload_length)
{
  if (String(topic) == "CK.Id")                                           // Check if the received topic is "CK.Id".
  {
    char sub_frame[payload_length - CKIdOffset + 1];                      // Create a buffer to store the sub_frame extracted from the payload.
    bool ckConfigured = false;                                            // Flag to indicate if the CK device is configured.
    Serial.println("");
    Serial.println("TOPIC RECEIVED : CK.Id");                             // Print a message to indicate that the topic "CK.Id" is received.
    Serial.print("payload_length = ");
    Serial.println(payload_length);
    Serial.print("CKIdOffset = ");
    Serial.println(CKIdOffset);
    Serial.print("payload_length - CKIdOffset + 1 = ");
    Serial.println(payload_length - CKIdOffset + 1);
    Serial.print("topic = ");
    Serial.println(String(topic));
    Serial.print("payload = ");
    Serial.println(String(payload));    

    for (int i = 0; i < (payload_length - CKIdOffset + 1); i++)           // Extract the sub_frame from the payload starting from the CKIdOffset.
    {
      sub_frame[i] = payload[i + CKIdOffset];
      Serial.print(i);                                                    // Print the sub_frame byte by byte for debugging purposes.
      Serial.print(" = ");                                                // iterates i = subframe[i] , HEX = subframe[i] in hex and move new line.
      Serial.print(sub_frame[i]);
      Serial.print(" , HEX = ");
      Serial.println(sub_frame[i], HEX);
    }

    OEEdata_copy = String(sub_frame);                                     // Convert the sub_frame to a String.
    Serial.println("");
    Serial.print("OEEdata_copy = ");
    Serial.println(OEEdata_copy);                                         // Print the complete sub_frame for debugging purposes.

    for (int i = 0; i < 12; i++)                                          // Process the sub_frame to identify the position of the CK device based on its MAC address.
    {
      indexComma = OEEdata_copy.indexOf(',');                             // Find the position of the comma (',') in the sub_frame.
      Serial.println("");
      Serial.print("Iteration: ");
      Serial.println(i);
      Serial.print("indexComma = ");
      Serial.println(indexComma);
      SubOEEdata = OEEdata_copy.substring(0, indexComma + 1);             // Extract the substring before the comma.
      Serial.println("SubOEEdata = " + SubOEEdata);                       // Print the extracted substring for debugging purposes.

      if (SubOEEdata == ("CK-" + esp32_macAddress + ","))                 // Check if the extracted substring matches the current CK device's MAC address(ID).
      {
        ckConfigured = true;                                              // Set the ckConfigured flag to true, indicating the CK device is configured.
        Serial.println("");
        Serial.print("CK found at position = ");                          // Print the position of the CK device.
        Serial.println(i + 1);

        if (i == 10)                                                      // Depending on the position, perform specific actions: Position 11
        {
          Serial.println("CK Pass - CK Pass flag enabled");               // CK is a pass device.
          CK_Pass_flag = true;                                            // Set the CK_Pass_flag to true.
          memory.putBool("CK_Pass_flag", CK_Pass_flag);
        }
        else if (i == 11)                                                 // Depending on the position, perform specific actions: Position 12
        {
          Serial.println("CK Interlock - CK Interlock Enabled");          // CK is an interlock device.
          CK_Interlock_flag = true;                                       // Set the CK_Interlock_flag to true.
          memory.putBool("CK_Interlock_flag", CK_Interlock_flag);
        }
        else                                                              // CK is configured for counting. Generate the topic where the count data will be sent.
        {
          if ((i + 1) < 10)
          {
            CKCountTopic = "CK0" + String(i + 1) + ".Count";
          }
          else
          {
            CKCountTopic = "CK" + String(i + 1) + ".Count";
          }

          Serial.print("Count will be sent on topic = ");
          Serial.println(CKCountTopic);
          memory.putString("CKCountTopic", CKCountTopic);                 // Save CK configuration information to non-volatile memory.
          isConfigured = true;
          memory.putBool("isConfigured", isConfigured);
          Serial.println("Save CK config to non-volatile memory");
        }
      }
      OEEdata_copy.remove(0, indexComma + 1);                             // Remove the processed part of the sub_frame for the next iteration.
    }
    if (ckConfigured == true)                                             // Execute if the ckConfigured is true.
    {
      Serial.print("CK will publish to topic ");
      Serial.print(CKCountTopic);
      Serial.print(". ");
      if (CK_Pass_flag)
      {
        Serial.print("This is a pass CK. ");
      }
      else
      {
        Serial.print("");
      }
      if (CK_Interlock_flag)
      {
        Serial.println("This is an interlock CK. ");
      }
      else
      {
        Serial.println("");
      }
    }
    else
    {
      Serial.println("CK config has been cleared. Retransmitting.");      // CK configuration has been cleared. Print the status and retransmit.
      isConfigured = false;
      memory.putBool("isConfigured", isConfigured);
    }
  }

  if (String(topic) == "TPC.MachineStatus")
  {
    char sub_frame[payload_length - MachineStatusOffset + 1];
    Serial.println("Topic Received : TPC.MachineStatus");

    for (int i = 0; i < (payload_length - MachineStatusOffset + 1); i++)
    {
      sub_frame[i] = payload[i + MachineStatusOffset];
      Serial.print(sub_frame[i]);
    }

    if (String(sub_frame) == "Machine Setup")
    {
      Serial.println("Reset Counters");
      rawCount = 0;
      inProduction = false;
      memory.putULong64("count-raw", 0);
      memory.putFloat("count-calc", 0);
      currentMachineStatus = MachineSetup;
      memory.putInt("current-mstats", currentMachineStatus);
    }
    else if (String(sub_frame) == "No Production")
    {
      Serial.println("No Production");
      inProduction = false;
      currentMachineStatus = NoProduction;
      memory.putInt("current-mstats", currentMachineStatus);
    }
    else if (String(sub_frame) == "Production")
    {
      Serial.println("In Production Mode");
      inProduction = true;
      currentMachineStatus = Production;
      memory.putInt("current-mstats", currentMachineStatus);
    }
    else
    {
      Serial.println("Invalid Machine Status");
    }
  }
  else
  {
    Serial.println("incoming message on topic: " + String(topic) + " of size: " + String(payload_length));
    for (int i = 0; i < payload_length; i++) Serial.print(payload[i]);
    Serial.println("");
  }
}

boolean reconnect()
{
  if (client.connect(ck_id_buffer))
  {
    mqttConnected = true;
    Serial.println("CK sensor connected to OEE Base Station MQTT Broker");
    // Once connected, resubscribe to topic..
    if (client.subscribe("TPC.MachineStatus"))
    {
      Serial.println("CK sensor subscribed to TPC.MachineStatus");
    }
    else
    {
      Serial.println("Error subscribing to TPC.MachineStatus");
    }
    if (client.subscribe("CK.Id"))
    {
      Serial.println("CK sensor subscribed to CK.Id");
    }
    else
    {
      Serial.println("Error subscribing to CK.Id");
    }
    /*if (client.subscribe("CK.Defect"))
      {
      Serial.println("CK sensor subscribed to CK.Defect");
      }
      else
      {
      Serial.println("Error subscribing to CK.Defect");
      }
      if (client.subscribe("CK.Reject"))
      {
      Serial.println("CK sensor subscribed to CK.Reject");
      }
      else
      {
      Serial.println("Error subscribing to CK.Reject");
      }
      return client.connected();
      // ... and publish to Engine.Discovery*/
    current_state = STATE_CONNECT_MQTT;
  }
  return client.connected();
}

void setup()
{
  // WIFI CONNECT
  WiFi.disconnect(true);  // clear previous wifi config
  delay(3000);
  pinMode(sensor1Pin, INPUT);
  pinMode(sensor2Pin, INPUT); //Tambah Baru
  //attachInterrupt(sensor1Pin, sensor1Trigger, CHANGE);
  Serial.begin(115200);
  Serial.println("VSys Counting Kit");
  Serial.print("FW version: ");
  Serial.println(FW_version);
  memory.begin("last-value", false);
  //TEMPORARY
  WIFI_SSID = "VSysOEE-STR-02";
  //Nama wifi router atas BS
  memory.putString("wifi-ssid", WIFI_SSID);
  WIFI_Password = "vsysoee@1";
  //Password wifi router atas BS
  memory.putString("wifi-pass", WIFI_Password);
  MQTT_Broker = "192.168.10.100";
  //IP Address SbRIO
  memory.putString("mqtt-broker", MQTT_Broker);
  MQTT_Port = "1883";
  memory.putString("mqtt-port", MQTT_Port);
  MachineStatusOffset = 32;
  memory.putULong64("mach-stats-offset", MachineStatusOffset);
  CKIdOffset = 16;
  memory.putULong64("ckid-offset", CKIdOffset);
  isConfigured = true;
  memory.putBool("isConfigured", isConfigured);
  lastOperation = HOME_COUNTING;
  memory.putString("last-oper", lastOperation);
  CKCountTopic = "CK03.Count";
  memory.putString("CKCountTopic", CKCountTopic);
  CK_Pass_flag = false;
  memory.putBool("CK_Pass_flag", CK_Pass_flag);
  CK_Interlock_flag = false;
  memory.putBool("CK_Interlock_flag", CK_Interlock_flag);
  currentMachineStatus = memory.getInt("current-mstats", 0);
  MachineStatusOffset = memory.getULong64("mach-stats-offset", 32);
  CKIdOffset = memory.getULong64("ckid-offset", 16);
  WIFI_SSID = memory.getString("wifi-ssid", "VSysOEE-STR-02");
  WIFI_Password = memory.getString("wifi-pass", "vsysoee@1");
  MQTT_Broker = memory.getString("mqtt-broker", "192.168.10.100");
  MQTT_Port = memory.getString("mqtt-port", "1883");
  CK_OWN_ID = memory.getString("ck-id", "CK-xx");
  countMultiplier = memory.getFloat("count-mul", 1);
  CountUnit = memory.getString("count-unit", "pcs");
  maxValue = memory.getFloat("max-val", 1);
  minValue = memory.getFloat("min-val", 0);
  WeightUnit = memory.getString("weight-unit", "kg");
  rawCount = memory.getULong64("count-raw", 0);
  calculatedCount = memory.getFloat("count-calc", 0);
  lastOperation = memory.getString("last-oper", "");
  isConfigured = memory.getBool("isConfigured", false);
  if (isConfigured)
  {
    CKCountTopic = memory.getString("CKCountTopic", CKCountTopic);
    CK_Pass_flag = memory.getBool("CK_Pass_flag", CK_Pass_flag);
    CK_Interlock_flag = memory.getBool("CK_Interlock_flag", CK_Interlock_flag);
  }
  randomSeed(analogRead(0));
  myNex.begin(9600);
  delay(1000);
  myNex.writeStr(SPLASH_SCREEN);
  myNex.lastCurrentPageId = 1;
  pageRefreshTimer = millis();
  // Create event handler for wifi events
  WiFi.onEvent(WiFiGotIP, WiFiEvent_t::ARDUINO_EVENT_WIFI_STA_GOT_IP);
  WiFi.onEvent(WiFiConnected, WiFiEvent_t::ARDUINO_EVENT_WIFI_STA_CONNECTED);
  WiFi.onEvent(WiFiDisconnected, WiFiEvent_t::ARDUINO_EVENT_WIFI_STA_DISCONNECTED);
  Serial.print("Connecting to SSID ");
  Serial.println(WIFI_SSID);
  Serial.print("With Password ");
  Serial.println(WIFI_Password);
  WIFI_SSID.toCharArray(ssid_buffer, WIFI_SSID.length() + 1);
  WIFI_Password.toCharArray(pass_buffer, WIFI_Password.length() + 1);
  WiFi.begin(ssid_buffer, pass_buffer);
  Serial.println("Waiting for Wifi..");
}

void loop()
{
  myNex.NextionListen();
  refreshCurrentPage();
  if (wifiConnected)
  {
    if (!client.connected())
    {
      mqttConnected = false;
      if (millis() - prevMQTTReconnectAttempt > MQTTReconnectInterval)
      {
        prevMQTTReconnectAttempt = millis();
        // Attempt to reconnect
        Serial.print("Connecting to MQTT broker at: ");
        Serial.println(MQTT_Broker);
        if (reconnect())
        {
          prevMQTTReconnectAttempt = 0;
        }
      }
    }
    else
    {
      client.loop();
    }
  }
  switch (current_state)
  {
    case STATE_INIT: //STATE_INIT done
      {
        break;
      }
    case STATE_CONNECT_WIFI:
      {
        if (wifiConnected)
        {
          current_state = STATE_CONNECT_MQTT;
        }
        break;
      }
    case STATE_CONNECT_MQTT: //STATE_INIT done
      {
        if (client.publish("Engine.Discovery", "CK"))
        {
          Serial.println("Message published to Engine.Discovery");
        }
        else
        {
          Serial.println("Error publishing to Engine.Discovery");
        }
        if (lastOperation == "")
        {
          current_state = STATE_WAITING; //waiting for user selection
          myNex.writeStr(SENSING_DEVICE);
        }
        else
        {
          myNex.writeStr(lastOperation);
          delay(50);
          if (lastOperation == HOME_COUNTING)
          {
            myNex.writeStr("t1.txt", "DIGITAL CK");
            myNex.writeStr("t0.txt", CountUnit);
            current_state = STATE_HOME_COUNTING;
          }
          else if (lastOperation == HOME_LOADCELL)
          {
            myNex.writeStr("t1.txt", "LOAD CELL");
            myNex.writeStr("t0.txt", WeightUnit);
            current_state = STATE_HOME_LOADCELL;
          }
          
        }
        break;
      }
    case STATE_HOME_COUNTING:
      {
        if (isConfigured)
        {
          calculatedCount = rawCount * countMultiplier;
          if (calculatedCount != oldCalculatedCount)
          {
            Serial.print("rawCount: ");
            Serial.println(rawCount);
            Serial.print("calculatedCount: ");
            Serial.println(calculatedCount);
            myNex.writeStr("t2.txt", String(calculatedCount));
            memory.putULong64("count-raw", rawCount);
            memory.putFloat("count-calc", calculatedCount);
            if (millis() - publishMillis > publishMQTTInterval)
            {
              if (mqttConnected)
              {
                Serial.println("Publishing count to: " + CKCountTopic);
                if (client.publish(CKCountTopic, String(calculatedCount)))
                {
                  Serial.println("Publish ok");
                }
                else
                {
                  current_state = STATE_ERROR;
                  error_code = ERROR_PUB_MQTT;
                  Serial.println("Error publishing to: " + CKCountTopic);
                }
                if (CK_Pass_flag)
                {
                  Serial.println("Publishing count to: CK.Pass");
                  if (client.publish("CK.Pass", String(calculatedCount)))
                  {
                    Serial.println("Publish ok");
                  }
                  else
                  {
                    current_state = STATE_ERROR;
                    error_code = ERROR_PUB_MQTT;
                    Serial.println("Error publishing to CK.Pass");
                  }
                }
              }
              publishMillis = millis();
            }
            oldCalculatedCount = calculatedCount;
          }
        }
        else
        {
          //CK sensor not yet configured, let's send MAC+IP to OEE Base Station
          if (millis() - SendConfigurationTimer > SEND_CONFIGURATION_INTERVAL)
          {
            Serial.println("Publishing CK-" + esp32_macAddress + "," + esp32_IPAddress + " to CK.Discover");
            WebSerial.println("Publishing CK-" + esp32_macAddress + "," + esp32_IPAddress + " to CK.Discover");
            if (mqttConnected)
            {
              if (client.publish("CK.Discover", ("CK-" + esp32_macAddress + "," + esp32_IPAddress)))
              {
                Serial.println("Publish ok");
              }
              else
              {
                current_state = STATE_ERROR;
                error_code = ERROR_PUB_MQTT;
                Serial.println("Error publishing to CK.Discover");
              }
            }
            SendConfigurationTimer = millis();
          }
        }
      }
      break;
    case STATE_HOME_LOADCELL:
      {
      }
      break;
    case STATE_WAITING: // TODO: add a timeout or watchdog
      {
        if (millis() - lastMillis_waiting_state > 1000) //non blocking delay for debug purpose
        {
          lastMillis_waiting_state = millis();
          Serial.println("Waiting for input from user...");
        }
      }
      break;
    case STATE_ERROR: // STATE_WAITING
      {
        Serial.println("Error: something went wrong");
        Serial.print("Error code: "); Serial.println(error_code);
        if (error_code == ERROR_CONNECT_WIFI)
        {
          current_state = STATE_CONNECT_WIFI;
        }
        else if (error_code == ERROR_CONNECT_MQTT)
        {
          mqttConnected = false;
          current_state = STATE_CONNECT_MQTT;
        }
        else if (error_code == ERROR_PUB_MQTT)
        {
          mqttConnected = false;
          current_state == STATE_CONNECT_MQTT;
        }
        else if (error_code == ERROR_SUB_MQTT)
        {
          mqttConnected = false;
          current_state == STATE_CONNECT_MQTT;
        }
        else
        {
          current_state = STATE_INIT;
        }
        delay(5000);
      }
      break;

    default:
      {
        break;
      }
  }
  if (Serial.available() > 0)
  {
    inByte = Serial.read();
    switch (inByte)
    {
      case '1'://
        {
          if (debug == 0)debug = 1;
          else debug = 0;
          Serial.print("debug = ");
          Serial.println(debug);
        }
        break;
      case '2':// Disable Measure debounce time (for debug)
        {
          if (debug_debounce == 0)debug_debounce = 1;
          else debug_debounce = 0;
          Serial.print("debug_debounce = ");
          Serial.println(debug_debounce);
        }
        break;
      case '3':// test debug isConfigured = memory.getBool("isConfigured", false);
        {
          Serial.print(digitalRead(sensor1Pin));
        }
        break;
      case '4':// Reset CK configuration (position)
        {
          if (isConfigured == true)
          {
            isConfigured = false;
          }
          else
          {
            isConfigured = true;
          }
          Serial.print("isConfigured: ");
          Serial.println(isConfigured);
          memory.putBool("isConfigured", isConfigured);
        }
        break;
      case '0':// Display CK ID
        {
          Serial.print("CK ID : ");
          Serial.println("CK-" + esp32_macAddress);
          Serial.print("IP Address : ");
          Serial.println(esp32_IPAddress);
          Serial.print("Machine Status Offset : ");
          Serial.println(MachineStatusOffset);
          Serial.print("CK ID Offset : ");
          Serial.println(CKIdOffset);
          Serial.print("CK Count Topic : ");
          Serial.println(CKCountTopic);
          Serial.print("Current Machine Status : ");
          if (currentMachineStatus == NoProduction)
          {
            Serial.println("No Production");
          }
          else if (currentMachineStatus == Production)
          {
            Serial.println("Production");
          }
          else if (currentMachineStatus == MachineSetup)
          {
            Serial.println("Machine Setup / New Part");
          }
        }
        break;
      case 'r': //Reset Count raw manually
        {
          Serial.println("Reset calculatedCount and rawCount");
          memory.putULong64("count-raw", 0);
          rawCount = memory.getULong64("count-raw", 0);
          memory.putFloat("count-calc", 0);
          calculatedCount = memory.getFloat("count-calc", 0);
          Serial.print("count-raw: "); Serial.println(rawCount);
          Serial.print("calculatedCount: "); Serial.println(calculatedCount);
        }
        break;
      case 'k': //For testting purpose
        {
          uint8_t result_pub;
          Serial.println("Publishing CK-0CB815C34700 to CK.Discover");
          result_pub = client.publish("CK.Discover", String("CK-" + esp32_macAddress + "," + esp32_IPAddress));
          if (result_pub)
          {
            Serial.println("Publish ok");
          }
          else
          {
            Serial.println("Publish Not ok");
          }
        }
        break;
      case 'z':
      case 'Z':
        {
          if (MachineStatusOffset < 9999)MachineStatusOffset++;
          Serial.print("Machine Status Offset = ");
          memory.putULong64("mach-stats-offset", MachineStatusOffset);
          Serial.println(memory.getULong64("mach-stats-offset", 32));
        }
        break;
      case 'x':
      case 'X':
        {
          if (MachineStatusOffset > 0)MachineStatusOffset--;
          Serial.print("Machine Status Offset = ");
          memory.putULong64("mach-stats-offset", MachineStatusOffset);
          Serial.println(memory.getULong64("mach-stats-offset", 32));
        }
        break;
      case 'c':
      case 'C':
        {
          if (CKIdOffset < 9999)CKIdOffset++;
          Serial.print("CK ID Offset = ");
          memory.putULong64("ckid-offset", CKIdOffset);
          Serial.println(memory.getULong64("ckid-offset", 16));
        }
        break;
      case 'v':
      case 'V':
        {
          if (CKIdOffset > 0)CKIdOffset--;
          Serial.print("CK ID Offset = ");
          memory.putULong64("ckid-offset", CKIdOffset);
          Serial.println(memory.getULong64("ckid-offset", 16));
        }
        break;
    }
  }
  int reading = digitalRead(sensor1Pin); //Tambah Baru [Update total count]
  int reading2 = digitalRead(sensor2Pin);
  if (reading != lastButtonState)
  {
    // reset the debouncing timer
    lastDebounceTime = millis();
  }
  if ((millis() - lastDebounceTime) > debounceDelay)
  {
    // whatever the reading is at, it's been there for longer than the debounce
    // delay, so take it as the actual current state:
    // if the button state has changed:
    if (reading != buttonState)
    {
      buttonState = reading;
      // only toggle the LED if the new button state is HIGH
      if (buttonState == HIGH || buttonState2 == HIGH)
      {
        if (currentMachineStatus == Production)
        {
          rawCount++;
        }
        else if (currentMachineStatus == NoProduction)
        {
          Serial.println("Sensor detect in no production");
        }
        else
        {
          Serial.println("Sensor detect in machine setup");
        }
      }
    }
  }
  if (reading2 != lastButtonState2)
  {
    lastDebounceTime2 = millis();
  }
  if ((millis() - lastDebounceTime2) > debounceDelay2)
  {
    if (reading2 != buttonState2)
    {
      buttonState2 = reading2;
      if (buttonState2 == HIGH)
      {
        if (currentMachineStatus == Production)
        {
          rawCount++;
        }
        else if (currentMachineStatus == NoProduction)
        {
          Serial.println("Sensor 2 detect in no production");
        }
        else
        {
          Serial.println("Sensor 2 detect in machine setup");
        }
      }
    }
  }
  lastButtonState = reading;
  lastButtonState2 = reading2;
  if (webSerialMsgAvailable)
  {
    // HANDLE MESSAGE COMING FROM WEB SERIAL
    webSerialMsgAvailable = false;
  }
  checkMQTTConnection();
}

void checkMQTTConnection()
{
}

void trigger0()
{
  myNex.writeStr(HOME_COUNTING);
  delay(50);
  myNex.writeStr("t1.txt", "DIGITAL CK");
  myNex.writeStr("t0.txt", CountUnit);
  memory.putString("last-oper", HOME_COUNTING);
  lastOperation = HOME_COUNTING;
  current_state = STATE_HOME_COUNTING;
}

void trigger1()
{
  myNex.writeStr(HOME_LOADCELL);
  delay(50);
  myNex.writeStr("t1.txt", "LOAD CELL");
  myNex.writeStr("t0.txt", WeightUnit);
  memory.putString("last-oper", HOME_LOADCELL);
  lastOperation = HOME_LOADCELL;
  current_state = STATE_HOME_LOADCELL;
}

void trigger2()
{
  prevPage = HOME_COUNTING;
  myNex.writeStr(WARNING_BACK);
  delay(50);
  myNex.writeStr("t0.txt", "");
  myNex.writeStr("t1.txt", "");
}

void trigger3()
{
  prevPage = HOME_COUNTING;
  myNex.writeStr(WARNING_CONFIG);
  delay(50);
  myNex.writeStr("t0.txt", "");
  myNex.writeStr("t1.txt", "");
}

void trigger4()
{
  prevPage = HOME_LOADCELL;
  myNex.writeStr(WARNING_BACK);
  delay(50);
  myNex.writeStr("t0.txt", "");
  myNex.writeStr("t1.txt", "");
}

void trigger5()
{
  prevPage = HOME_LOADCELL;
  myNex.writeStr(WARNING_CONFIG);
  delay(50);
  myNex.writeStr("t0.txt", "");
  myNex.writeStr("t1.txt", "");
}

void trigger6()
{
  WarningPassword = myNex.readStr("t0.txt");
  if (WarningPassword == PASSWORD)
  {
    myNex.writeStr(SENSING_DEVICE);
    myNex.writeStr("t1.txt", "");
    delay(50);
  }
  else
  {
    myNex.writeStr("t1.txt", WRONG_PASS_MSG);
  }
}

void trigger7()
{
  myNex.writeStr(prevPage);
  delay(50);
  if (prevPage == HOME_COUNTING)
  {
    myNex.writeStr("t1.txt", "DIGITAL CK");
    myNex.writeStr("t0.txt", CountUnit);
  }
  else if (prevPage == HOME_LOADCELL) {
    myNex.writeStr("t1.txt", "LOAD CELL");
    myNex.writeStr("t0.txt", WeightUnit);
  }
}

void trigger8()
{
  WarningPassword = myNex.readStr("t0.txt");
  if (WarningPassword == PASSWORD)
  {
    if (prevPage == HOME_COUNTING)
    {
      myNex.writeStr(CONFIG_COUNT);
      delay(100);
      myNex.writeStr("t2.txt", WIFI_SSID);
      myNex.writeStr("t3.txt", WIFI_Password);
      myNex.writeStr("t4.txt", MQTT_Broker);
      myNex.writeStr("t5.txt", MQTT_Port);
      myNex.writeStr("t6.txt", CountUnit);
      myNex.writeStr("t7.txt", CK_OWN_ID);
      myNex.writeNum("x0.val", countMultiplier);
    }
    else if (prevPage == HOME_LOADCELL)
    {
      myNex.writeStr(CONFIG_LC);
      delay(100);
      myNex.writeStr("t2.txt", WIFI_SSID);
      myNex.writeStr("t3.txt", WIFI_Password);
      myNex.writeStr("t4.txt", MQTT_Broker);
      myNex.writeStr("t5.txt", MQTT_Port);
      myNex.writeStr("t6.txt", WeightUnit);
      myNex.writeStr("t7.txt", CK_OWN_ID);
      myNex.writeNum("x1.val", maxValue);
      myNex.writeNum("x2.val", minValue);
    }
    myNex.writeStr("t1.txt", "");
  }
  else
  {
    myNex.writeStr("t1.txt", WRONG_PASS_MSG);
  }
}

void trigger9()
{
  myNex.writeStr(prevPage);
  delay(50);
  if (prevPage == HOME_COUNTING)
  {
    myNex.writeStr("t1.txt", "DIGITAL CK");
    myNex.writeStr("t0.txt", CountUnit);
  }
  else if (prevPage == HOME_LOADCELL)
  {
    myNex.writeStr("t1.txt", "LOAD CELL");
    myNex.writeStr("t0.txt", WeightUnit);
  }
}

void trigger10()
{
  if (prevPage == HOME_COUNTING)
  {
    WIFI_SSID = myNex.readStr("t2.txt");
    memory.putString("wifi-ssid", WIFI_SSID);
    WIFI_Password = myNex.readStr("t3.txt");
    memory.putString("wifi-pass", WIFI_Password);
    MQTT_Broker = myNex.readStr("t4.txt");
    memory.putString("mqtt-broker", MQTT_Broker);
    MQTT_Port = myNex.readStr("t5.txt");
    memory.putString("mqtt-port", MQTT_Port);
    CountUnit = myNex.readStr("t6.txt");
    memory.putString("count-unit", CountUnit);
    CK_OWN_ID = myNex.readStr("t7.txt");
    memory.putString("ck-id", CK_OWN_ID);
    countMultiplier = myNex.readNumber("x0.val");
    memory.putFloat("count-mul", countMultiplier); ///!!! to investigate why the Nextion library return 1000.00 instead of 1.00
  }
  else if (prevPage == HOME_LOADCELL)
  {
    WIFI_SSID = myNex.readStr("t2.txt");
    memory.putString("wifi-ssid", WIFI_SSID);
    WIFI_Password = myNex.readStr("t3.txt");
    memory.putString("wifi-pass", WIFI_Password);
    MQTT_Broker = myNex.readStr("t4.txt");
    memory.putString("mqtt-broker", MQTT_Broker);
    MQTT_Port = myNex.readStr("t5.txt");
    memory.putString("mqtt-port", MQTT_Port);
    WeightUnit = myNex.readStr("t6.txt");
    memory.putString("weight-unit", WeightUnit);
    CK_OWN_ID = myNex.readStr("t7.txt");
    memory.putString("ck-id", CK_OWN_ID);
    maxValue = myNex.readNumber("x1.val");
    memory.putFloat("max-val", maxValue);
    minValue = myNex.readNumber("x2.val");
    memory.putFloat("minVal", minValue);
  }
  myNex.writeStr(SAVE_CONFIG);
  delay(50);
}

void trigger11()
{
  myNex.writeStr(prevPage);
  delay(50);
  if (prevPage == HOME_COUNTING)
  {
    myNex.writeStr("t1.txt", "DIGITAL CK");
    myNex.writeStr("t0.txt", CountUnit);
  }
  else if (prevPage == HOME_LOADCELL)
  {
    myNex.writeStr("t1.txt", "LOAD CELL");
    myNex.writeStr("t0.txt", WeightUnit);
  }
}

void trigger12()
{
  myNex.writeStr(DEBUG_PAGE);
  delay(50);
}

void trigger13()
{
  myNex.writeStr(STATUS_PAGE);
  delay(50);
}

void trigger14()
{
  myNex.writeStr(lastOperation);
  delay(50);
  if (lastOperation == HOME_COUNTING)
  {
    myNex.writeStr("t1.txt", "DIGITAL CK");
    myNex.writeStr("t0.txt", CountUnit);
    current_state = STATE_HOME_COUNTING;
  }
  else if (lastOperation == HOME_LOADCELL)
  {
    myNex.writeStr("t1.txt", "LOAD CELL");
    myNex.writeStr("t0.txt", WeightUnit);
    current_state = STATE_HOME_LOADCELL;
  }
}

void trigger15()
{
  myNex.writeStr(lastOperation);
  delay(50);
  if (lastOperation == HOME_COUNTING)
  {
    myNex.writeStr("t1.txt", "DIGITAL CK");
    myNex.writeStr("t0.txt", CountUnit);
    current_state = STATE_HOME_COUNTING;
  }
  else if (lastOperation == HOME_LOADCELL)
  {
    myNex.writeStr("t1.txt", "LOAD CELL");
    myNex.writeStr("t0.txt", WeightUnit);
    current_state = STATE_HOME_LOADCELL;
  }
}

void refreshCurrentPage()
{
  if (millis() - pageRefreshTimer > PAGE_REFRESH_INTERVAL)
  {
    //Serial.println (myNex.currentPageId);
    switch (myNex.currentPageId)
    {
      case 0: //SPLASH SCREEN
        break;
      case 1: //SENSING DEVICE
        break;
      case 2: //DIGITAL COUNTING
        /*myNex.writeNum("x0.val", calculatedCount);
          if (calculatedCount != oldCalculatedCount){
          memory.putULong64("count-raw", rawCount);
          memory.putFloat("count-calc", calculatedCount);
          oldCalculatedCount = calculatedCount;
          }*/
        break;
      case 3: //LOAD CELL
        //myNex.writeStr("t2.txt", calculatedWeight);
        break;
      case 4: //WARNING BACK
        break;
      case 5: //WARNING CONFIG
        break;
      case 6: //CONFIG COUNTING
        break;
      case 7: //CONFIG LOAD CELL
        break;
      case 8: //SAVE PAGE
        secondCounter++;
        if (secondCounter > 2)
        {
          myNex.writeStr(prevPage);
          delay(50);
          if (prevPage == HOME_COUNTING)
          {
            myNex.writeStr("t1.txt", "DIGITAL CK");
            myNex.writeStr("t0.txt", CountUnit);
          }
          else if (prevPage == HOME_LOADCELL)
          {
            myNex.writeStr("t1.txt", "LOAD CELL");
            myNex.writeStr("t0.txt", WeightUnit);
          }
          secondCounter = 0;
        }
        break;
      case 9: //STATUS
        if (wifiConnected)
        {
          myNex.writeStr("t0.txt", "WIFI Connected");
          myNex.writeStr("t2.txt", esp32_IPAddress);
        }
        else
        {
          myNex.writeStr("t0.txt", "WIFI Not Connected");
          myNex.writeStr("t2.txt", "-.-.-.-");
        }
        if (mqttConnected)
        {
          myNex.writeStr("t1.txt", MQTT_Broker + ":" + MQTT_Port);
        }
        else
        {
          myNex.writeStr("t1.txt", "MQTT Not Connected");
        }
        break;
      case 10:
        myNex.writeStr("t0.txt", MQTTMessage);
        break;
      default:
        break;
    }
    pageRefreshTimer = millis();
  }
}
